resource "google_compute_subnetwork" "custom-subnetwork-ir-gke" {
  name          = "custom-subnetwork-ir-gke"
  ip_cidr_range = "10.50.0.0/16"
  region        = var.region
  network       = google_compute_network.custom-vpc-ir-gke.self_link
  secondary_ip_range {
    range_name    = "custom-subnetwork-ir-gke-secondary-range"
    ip_cidr_range = "192.168.50.0/24"
  }
}
resource "google_compute_network" "custom-vpc-ir-gke" {
  name                    = "custom-vpc-ir-gke"
  auto_create_subnetworks = false
}