resource "google_compute_firewall" "http-traffic-ir-gke" {
  name    = "allow-http-ir-gke"
  network = google_compute_network.custom-vpc-ir-gke.self_link
  allow {
    protocol = "tcp"
    ports    = ["80", "5000"]
}
  target_tags = ["http-traffic"]
}
resource "google_compute_firewall" "http-ssh-ir-gke" {
  name    = "allow-ssh-ir-gke"
  network = google_compute_network.custom-vpc-ir-gke.self_link
  allow {
    protocol = "tcp"
    ports    = ["22"]
}
  target_tags = ["ssh-traffic"]
}