resource "google_container_cluster" "ir-container-1" {
    provider = google-beta
    name = "ir-conatiner-1"
    location = var.location
    network = google_compute_network.custom-vpc-ir-gke.self_link
    subnetwork = google_compute_subnetwork.custom-subnetwork-ir-gke.self_link
    
    # We can't create a cluster with no node pool defined, but we want to only use
    # separately managed node pools. So we create the smallest possible default
    # node pool and immediately delete it.
    remove_default_node_pool = true
    initial_node_count = 1

private_cluster_config {
    enable_private_nodes = "false"
}
    # With a private cluster, it is highly recommended to restrict access to the cluster master
    # However, for testing purposes we will allow all inbound traffic.
  master_authorized_networks_config {
      cidr_blocks{
          cidr_block   = "0.0.0.0/0"
    display_name = "all for testing"
    } 
    }

    addons_config {
        http_load_balancing {
    disabled = false
    }
    horizontal_pod_autoscaling {
    disabled = false
    }
    }
    #not supported yet
    #vertical_pod_autoscaling = true

    master_auth {
        username = ""
        password = ""
        
    client_certificate_config {
        issue_client_certificate = false
        } 
        }

#logging
logging_service = "logging.googleapis.com/kubernetes"
monitoring_service = "monitoring.googleapis.com/kubernetes"

#maintainence 
maintenance_policy {
  daily_maintenance_window {
    start_time = "05:00"
    } 
    }
}
resource "google_container_node_pool" "primary_ir_node" {
  provider = google-beta
  name     = "ir-nodes--pool-primary"
  project  = var.project
  location = var.location
  cluster  = google_container_cluster.ir-container-1.name
  initial_node_count = "1"
  autoscaling {
    min_node_count = "1"
    max_node_count = "5"
}
  management {
    auto_repair  = "true"
    auto_upgrade = "true"
  }
  node_config {
    image_type   = "COS"
    machine_type = "n1-standard-1"
    disk_size_gb = 20
    disk_type    = "pd-ssd"
    # peemptiable true mean node will be replace every 24 hours suitable for stateless app and schedulerjobs
    # https://cloud.google.com/compute/docs/instances/preemptible
    preemptible = false
    metadata = {
      disable-legacy-endpoints = "true"
}
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      # privileges to read private image in GCR
      "https://www.googleapis.com/auth/devstorage.read_only",
]
 }
  lifecycle {
    ignore_changes = [initial_node_count]
}
  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
} 
}

resource "google_container_node_pool" "secondary_ir_node" {
  provider = google-beta
  name     = "ir-nodes-pool-secondary"
  project  = var.project
  location = var.location
  cluster  = google_container_cluster.ir-container-1.name
  initial_node_count = "1"
  autoscaling {
    min_node_count = "1"
    max_node_count = "5"
  }
  management {
    auto_repair  = "true"
    auto_upgrade = "true"
  }
  node_config {
    image_type   = "COS"
    machine_type = "n1-standard-1"
    disk_size_gb = 20
    disk_type    = "pd-ssd"
    # peemptiable true mean node will be replace every 24 hours suitable for stateless app and schedulerjobs
    # https://cloud.google.com/compute/docs/instances/preemptible
    preemptible = false
    metadata = {
      disable-legacy-endpoints = "true"
}
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      # privileges to read private image in GCR
      "https://www.googleapis.com/auth/devstorage.read_only",
] 
}
  lifecycle {
    ignore_changes = [initial_node_count]
}
  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
} 
}

