variable "project" {
  description = "The name of the GCP Project where all resources will be launched."
  default     = "ovo-id-poc"
}
variable "service_account_name" {
  description = "The name of the custom service account. This parameter is limited to a maximum of 28 characters."
  default     = "ir-service-account"
}
variable "service_account_description" {
  description = "The description of the custom service account."
  default     = "service account for ir-medan service"
}
variable "location" {
  description = "The location region for this gke cluster"
  default     = "asia-southeast1"
}
variable "region" {
  description = "The location region for this gke cluster"
  default     = "asia-southeast1"
}