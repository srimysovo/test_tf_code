terraform {
  backend "gcs" {
    bucket  = "my_bucket_infra_rangers"
    prefix  = "terraform/state/vpc"
  }
}