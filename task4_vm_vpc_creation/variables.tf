variable "project" {
  type = string
}
variable "zone" {
  type = string
}
variable "registry" {
  type = string
}
variable "username" {
  type = string
}
variable "password" {
  type = string
}
variable "image" {
  type = string
}