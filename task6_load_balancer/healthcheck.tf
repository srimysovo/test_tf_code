resource "google_compute_health_check" "global-healthcheck-ir" {
  name        = "global-healthcheck-ir1"
  description = "Health check via http"
  timeout_sec         = 1
  check_interval_sec  = 1
  healthy_threshold   = 4
  unhealthy_threshold = 5
  http_health_check {
    port         = 5000
    proxy_header = "NONE"
    request_path = "/"
} 
}