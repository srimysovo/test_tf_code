resource "google_compute_subnetwork" "network-with-private-secondary-ip-ranges-ir" {
    name = "test-subnetwork-ir"
    ip_cidr_range = "10.30.0.0/16"
    region = "asia-southeast1"
    network = google_compute_network.custom-test-ir-1.self_link

    secondary_ip_range {
        range_name = "tf-test-secondary-range-ir"
        ip_cidr_range = "192.168.10.0/24"
    }
}


resource "google_compute_network" "custom-test-ir-1" {
    name = "custom-test-network-ir-1"
    auto_create_subnetworks = "false"
  
}
