resource "google_compute_backend_service" "gloabl-backend-ir" {
name = "gloabl-backend-ir"
protocol = "HTTP"
port_name = "test-port-mig4-5000"
timeout_sec = 10
session_affinity = "NONE"

  backend {
    group = google_compute_instance_group.mig-2-ir.self_link
}
  health_checks = [google_compute_health_check. global-healthcheck-ir.self_link]
}