resource "google_compute_firewall" "global-firewall-ir" {
  ## firewall rules enabling the load balancer health checks
  name    = "global-firewall-ir"
  network = "default"
  description = "allow Google health checks and network load balancers access"
  allow {
    protocol = "icmp"
}
  allow {
    protocol = "tcp"
    ports    = ["5000"]
}
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["kecebong-air-target"]
}