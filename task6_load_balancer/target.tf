resource "google_compute_target_http_proxy" "global-proxy-ir" {
  name    = "global-proxy-ir-1"
  url_map = google_compute_url_map.global-map-ir.self_link
}
resource "google_compute_url_map" "global-map-ir" {
  name            = "global-map-ir-1"
  default_service = google_compute_backend_service.gloabl-backend-ir.self_link
}