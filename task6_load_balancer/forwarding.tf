resource "google_compute_global_forwarding_rule" "global-forwarding-ir" {
    name = "global-forwarding-ir-1"
    ip_address = google_compute_global_address.global-address-ir.address
    port_range = "80"
    target = google_compute_target_http_proxy.global-proxy-ir.self_link
}
