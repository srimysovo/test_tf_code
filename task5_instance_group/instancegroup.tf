resource "google_compute_instance_group" "mig-2-ir" {
    name = "mig-2-ir-ig"
    description = "terraform instnace groups sri"  

instances = [
    google_compute_instance.flask-server-docker2.self_link,
    google_compute_instance.flask-server-docker3.self_link,

]

named_port {
    name = "test-port-mig"
    port = "8080"
}

named_port{
    name = "test-port-ming2"
    port =443
}

named_port {
    name = "test-port-mig3"
    port = "80"
}
  named_port {
    name = "test-port-mig4"
    port = "5000"
}
  zone = "asia-southeast1-a"
}