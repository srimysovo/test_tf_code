provider "google"{
    version = "2.9.1"
    project = var.project
    zone = var.zone
}

resource "google_compute_firewall" "http-traffic-ir" {
    name = "allow-http-ir-mig"
    network = google_compute_network.custom-test-ir-1.self_link 

    allow {
        protocol = "tcp"
        ports = ["80","5000"]
    }

    target_tags = ["http-traffic-ir"]
}

resource "google_compute_firewall" "http-ssh-ir" {
    name = "allow-ssh-ir-mig"
    network = google_compute_network.custom-test-ir-1.self_link

    allow  {
        protocol = "tcp"
        ports = ["22"]
    }
    target_tags = ["ssh-traffic-ir"]
}

data "template_file" "cloud-init" {
    template = file("cloud-init.yml.tmpl")

    vars = {
        registry = var.registry
        username = var.username
        password = var.password
        image    = var.image
    }
}

resource "google_compute_instance" "flask-server-docker2" {
    name = "flask-server-docker2-${substr(filemd5("cloud-init.yml.tmpl"), 0, 10)}"
    machine_type = "n1-standard-1"

    tags = ["http-traffic-ir", "ssh-traffic-ir"]
    boot_disk {
    initialize_params {
      image = "cos-cloud/cos-73-11647-217-0"
    }
}

service_account {
    scopes = ["compute-ro", "storage-ro"]
}

network_interface {
    network = google_compute_network.custom-test-ir-1.self_link
    subnetwork = google_compute_subnetwork.network-with-private-secondary-ip-ranges-ir.self_link
    access_config {
      // this empty block creates a public IP address
} 
}
metadata = {
    "user-data" = data.template_file.cloud-init.rendered
} 
}

resource "google_compute_instance" "flask-server-docker3" {
    name = "flask-server-docker3-${substr(filemd5("cloud-init.yml.tmpl"), 0, 10)}"
    machine_type = "n1-standard-1"

    tags = ["http-traffic-ir", "ssh-traffic-ir"]
    boot_disk {
    initialize_params {
      image = "cos-cloud/cos-73-11647-217-0"
    }
}

service_account {
    scopes = ["compute-ro", "storage-ro"]
}

network_interface {
    network = google_compute_network.custom-test-ir-1.self_link
    subnetwork = google_compute_subnetwork.network-with-private-secondary-ip-ranges-ir.self_link
    access_config {
      // this empty block creates a public IP address
} 
}
metadata = {
    "user-data" = data.template_file.cloud-init.rendered
} 
}


output "ipaddress1" {
    value = google_compute_instance.flask-server-docker2.network_interface[0].access_config[0].nat_ip
}

output "ipaddress2" {
    value = google_compute_instance.flask-server-docker3.network_interface[0].access_config[0].nat_ip
}


